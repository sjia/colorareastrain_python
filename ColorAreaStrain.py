# Compute surface area change ratio between two meshes.

import vtk
import numpy as np
import os
import glob
import sys
from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy

def __read_vtk_polydata(inputfile):
    reader = vtk.vtkPolyDataReader()
    reader.SetFileName(inputfile)
    reader.Update()
    polydata = reader.GetOutput()
    return polydata

def __write_vtk_polydata(polydata,outputfile):
    writer = vtk.vtkPolyDataWriter()
    writer.SetFileName(outputfile);
    if vtk.VTK_MAJOR_VERSION <= 5:
        writer.SetInput(polydata)
    else:
        writer.SetInputData(polydata)
    writer.Write()

def __addCellScalar(polydata, np_label, name):
    if len(np_label) == polydata.GetNumberOfCells():
        label = numpy_to_vtk(np_label, deep=True)
        label.SetName(name)
        polydata.GetCellData().AddArray(label)
        # polydata.GetCellData().SetActiveScalars(name)      
    else:
        print('Error: mismatch of number of cells and scalar dimension.')

def __addPointScalar(polydata, np_label, name):
    if len(np_label) == polydata.GetNumberOfPoints():
        label = numpy_to_vtk(np_label, deep=True)
        label.SetName(name)
        polydata.GetPointData().AddArray(label)
        # polydata.GetPointData().SetActiveScalars(name)
    else:
        print('Error: mismatch of number of points and scalar dimension.')

def __calculate_area(polydata):
    n = polydata.GetNumberOfCells()
    np_area = np.zeros(n) 
    for i in range(n):
        cell = polydata.GetCell(i)
        np_area[i] = cell.ComputeArea()
    return np_area

def add_area_change(poly_0, poly_1, option=0):
    np_area_0 = __calculate_area(poly_0)
    np_area_1 = __calculate_area(poly_1)
    np_ac = (np_area_1-np_area_0)/np_area_0
    if poly_0.GetNumberOfPoints()!=poly_0.GetNumberOfPoints():
        print('Error: two input meshes have different number of points.')
    np_point_ac = np.zeros(poly_0.GetNumberOfPoints())
    np_count = np.zeros(poly_0.GetNumberOfPoints())
    for j in range(poly_0.GetNumberOfCells()):
        cell = poly_0.GetCell(j)
        p0 = cell.GetPointId(0)
        p1 = cell.GetPointId(1)
        p2 = cell.GetPointId(2)
        np_point_ac[p0] = np_point_ac[p0]+np_ac[j]
        np_point_ac[p1] = np_point_ac[p1]+np_ac[j]
        np_point_ac[p2] = np_point_ac[p2]+np_ac[j]
        np_count[p0] = np_count[p0]+1
        np_count[p1] = np_count[p1]+1
        np_count[p2] = np_count[p2]+1
    np_point_ac = np.divide(np_point_ac,np_count)
    np_point_ac[np_point_ac == -np.inf] = 0
    np_point_ac[np_point_ac == np.inf] = 0
    np_point_ac[np_point_ac == np.nan] = 0
    if option==0:
        __addPointScalar(poly_0, np.nan_to_num(np_point_ac), 'pointAreaChange')
    else:
        __addPointScalar(poly_1, np.nan_to_num(np_point_ac), 'pointAreaChange')

infile_0 = sys.argv[1]
infile_1 = sys.argv[2]
outfile = sys.argv[3]

mesh_0 = __read_vtk_polydata(infile_0)
mesh_1 = __read_vtk_polydata(infile_1)

add_area_change(mesh_0, mesh_1)

__write_vtk_polydata(mesh_0, outfile)

# example
# python ./ColoringAreaStrain.py input_filename_t0.vtk input_filename_t1.vtk output_filename.vtk
